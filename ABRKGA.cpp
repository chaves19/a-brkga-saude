using namespace std;

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <cstring>
#include <string.h>
#include <omp.h>
#include <algorithm>

#include "ABRKGA.h"


//------DEFINITION OF CONSTANTS, TYPES AND VARIABLES--------

// Run
char instance[50];                          // name of instance

//A-BRKGA
int n;                                      // size of cromossoms
int p;          	                        // size of population
int maxGenerations;                         // max number of generations
double pe;              	                // fraction of population to be the elite-set
double pm;          	                    // fraction of population to be replaced by mutants
double rhoe;             	                // probability that offspring inherit an allele from elite parent
double alfa;                                // threashold of restrict chromossoms list
double beta;                                // perturbation intensity
double sigma;                               // pearson correlation factor
double teta;								// percentage of generations in an epoch
double gama;								// cooling rate of the population

vector <TSol> Pop;                      	// current population
vector <TSol> PopInter;               		// intermediary population

TSol bestSolution;                          // best solution found in the A-BRKGA

// computational time
clock_t CPUbegin,							// initial run time
        CPUbest,							// time to find the best solution in a run
        CPUend;								// end run time

//Problem specific data
//(n is the number of tasks)
int m;                                      // number of teams
int M;                                      // sum of big initial time windows (tasks and teams) and maximum travel cost
int MW;                                     // minimum priority of a task
int MZ;                                     // sum of the difference between the end e initial time windows of teams

vector <vector <double> > c;				// matrix with travel cost between tasks
vector <vector <double> > d;				// matrix with travel cost between garage of teams and tasks
vector <vector <int> > q;				    // matrix with skills between tasks and teams

struct TItem								// struct with tasks informations
{
	int pt;                                 // execuction time
    int w;                                  // priority
    int a;                                  // begin time windows
    int b;                                  // end time windows
    double x;                               // coordinate x
    double y;                               // coordinate y
};

vector <TItem> task;                        // tasks vector
vector <TItem> team;                        // team vector

int print = 0;                              // print problem solution in the screen


/************************************************************************************
								FUNCTIONS AREA
*************************************************************************************/
int main()
{
    // file with test instances
	FILE *arqProblems;
    arqProblems = fopen ("arqProblems.txt", "r");

    if (arqProblems == NULL)
    {
        printf("\nERROR: File arqProblems.txt not found\n");
        getchar();
        exit(1);
    }

    // best solution that is saved in out file
    TSol sBest;

	// run the A-BRKGA for all test instances
	while (!feof(arqProblems))
	{
		// read the name of instances, debug mode, local search module, maximum time, maximum number of runs, maximum number of threads
		char nameTable[100];
		fscanf(arqProblems,"%s %d %d %d %d %d %d", nameTable, &debug, &ls, &MAXTIME, &MAXRUNS, &MAX_THREADS);

		//read the informations of the instance
        ReadData(nameTable);

        double foBest = INFINITY,
               foAverage = 0;

        float timeBest = 0,
              timeTotal = 0;

        // best solutions found in MAXRUNS
        sBest.fo = INFINITY;

		// run ABRKGA MaxRuns for each instance
        printf("\n\nInstance: %s \nRun: ", instance);
        for (int j=0; j<MAXRUNS; j++)
        {
            // fixed seed
            srand(j+1);

            printf("%d ", j+1);
            CPUbegin = CPUend = CPUbest = clock();

            // best solution found in this run
            bestSolution.fo = INFINITY;

            // execute the evolutionary method
            A_BRKGA();

            CPUend = clock();

            if (bestSolution.fo < sBest.fo)
                sBest = bestSolution;

            // calculate average results
            if (bestSolution.fo < foBest)
                foBest = bestSolution.fo;
            foAverage += bestSolution.fo;
            timeBest += (float)(CPUbest - CPUbegin)/CLOCKS_PER_SEC;
            timeTotal += (float)(CPUend - CPUbegin)/CLOCKS_PER_SEC;
        }

        // create a .xls file with average results
        foAverage = foAverage / MAXRUNS;
        timeBest = timeBest / MAXRUNS;
        timeTotal = timeTotal / MAXRUNS;

        if (!debug)
        {
        	   WriteSolution(sBest);
        	   WriteResults(foBest, foAverage, timeBest, timeTotal);
        }
        else
        {
            WriteSolutionScreen(sBest);
        }

        // free memory of problem variables
        FreeMemory();
    }

    fclose(arqProblems);
    return 0;
}

/************************************************************************************
			                  PROBLEM SPECIFIC FUNCTIONS
*************************************************************************************/

TSol Decoder(TSol s)
{
    // save the random-key sequence of current solution
    TSol temp = s;

    // create a initial solution of the problem
    s.fo = 0;
    for (int i=0; i<n; i++)
        s.vec[i].sol = i;

    // sort random-key vector
    sort(s.vec.begin(), s.vec.end()-2, sortByRk);

    // calculate objective function
    s.fo = CalculateFO(s);

    // return initial random-key sequence
    for (int i=0; i<n; i++)
        s.vec[i].rk = temp.vec[i].rk;

    return s;
}

double CalculateFO(TSol s)
{
    vector < vector <int> >  route;     // matriz com a sequencia de tarefas a serem realizadas por cada equipe
    vector <double> beginTime;          // vetor com o tempo de inicio de cada tarefa
    vector <double> travelTime;          // vetor com o tempo total percorrido na rota
    vector <double> z;                        // vetor com os tempos ociosos das equipes
    
    // initialize vectors
    beginTime.resize(n, -1);
    route.resize(m);
    travelTime.resize(m, 0);
    z.resize(m);
    
    // inicialmente as equipes possuem todo o tempo da janela como ociosa
    for (int j=0; j<m; j++)
        z[j] = team[j].b - team[j].a;       
    
    // find the best team and position to insert task s.vec[i].sol
    for (int i=0; i<n; i++)
    {
        int achei = 0;                                          // achar alguma equipe que possa realizar a tarefa sol[i] 
        
        // global variables
        int bestTeam = 0;                                  // melhor equipe para fazer a tarefa
        int bestPos = 0;                                     // posicao da rota onde inserir a tarefa sol[i] na bestTeam
        double bestTime = 9999999;                  // tempo de viagem da melhor equipe
        double bestZ = 0;                                  // tempo ocioso da melhor equipe
        double bestInicio = 0;                            // tempo de inicio da tarefa sol[i] no melhor team e posicao
        
        // team variables
        int posStarTeam = 0;                              // melhor posicao para inserir a tarefa i na rota j 
        double tStarTeam = 9999999;                 // melhor tempo de uma rota j com a insercao da tarefa i
        double zStarTeam = -99999999;             // tempo ocioso da melhor posicao de insercao na rota j
        double inicioStarTeam = 0;                     // tempo no qual irei comecar a tarefa sol[i] na rota j

        // verify if is possible to insert task sol[i] in the team j
        for (int j=0; j<m; j++)
        {  
            // team j has skill to perform task s.vec[i].sol
            if (q[s.vec[i].sol][j] == 1)
            {
                // *** find best position to insert task s.vec[i].sol
                
                // route variables
                int posRoute = 0;                          // define a posicao de insercao da tarefa i na equipe j na posicao k
                double zStarRoute = 0;                 // define o melhor tempo ocioso da equipe j
                double tStarRoute = 0;                 // define a melhor alteracao no tempo de viagem da equipe j
                double inicioStarRoute = 0;           // define o melhor inicio para a tarefa i na rota j

                // iniciar variaveis das equipes
                posStarTeam = 0;                          // melhor posicao na rota j
                tStarTeam = 9999999;                   // melhor tempo na rota j
                
                int existe = 0;                               // there is an feasible position to insert task i in the route j
                double inicio = 0;                          // define o tempo de inicio da tarefa i na equipe j na posicao k
                
                // verificar se posso inserir em todas posicoes da rota j (inclusive no fim)
                for (int k=0; k<=route[j].size(); k++)
                {
                    // rota esta vazia
                    if (route[j].empty())
                    {
                        // definir inicio da tarefa
                        if (team[j].a+d[j][s.vec[i].sol] <= task[s.vec[i].sol].a)       // chegar antes da janela de inicio da tarefa (esperar)
                            inicio = task[s.vec[i].sol].a;
                        else                                                                     // chegar depois da janela de inicio da tarefa estar aberta
                            inicio = team[j].a+d[j][s.vec[i].sol]; 

                        // checar a janela de fim da tarefa e da equipe
                        if ((inicio + task[s.vec[i].sol].pt <= task[s.vec[i].sol].b) && (inicio + task[s.vec[i].sol].pt + d[j][s.vec[i].sol] <= team[j].b))
                        {
                            if (team[j].b - (inicio + task[s.vec[i].sol].pt + d[j][s.vec[i].sol]) > zStarRoute)
                            {
                                zStarRoute = team[j].b - (inicio + task[s.vec[i].sol].pt + d[j][s.vec[i].sol]);      // tempo ocioso
                                tStarRoute = d[j][s.vec[i].sol] + d[j][s.vec[i].sol];                                          // tempo de viagem (rota vazia inicialmente)
                                
                                existe = 1;
                                posRoute = 0;
                                inicioStarRoute = inicio;
                            }
                        }
                    }
                    
                    // inserir antes da primeira tarefa (nao altera o tempo ocioso)
                    else
                    if (k==0)
                    {
                        // definir o inicio
                        if (team[j].a+d[j][s.vec[i].sol] <= task[s.vec[i].sol].a)       // chegar antes da janela de inicio da tarefa (esperar)
                            inicio = task[s.vec[i].sol].a;
                        else                                                                     // chegar depois da janela de inicio da tarefa estar aberta
                            inicio = team[j].a+d[j][s.vec[i].sol]; 
                        
                        // verificar a janela de tempo
                        if ((inicio + task[s.vec[i].sol].pt <= task[s.vec[i].sol].b) && ((inicio + task[s.vec[i].sol].pt + c[s.vec[i].sol][route[j][k]]) <= beginTime[route[j][k]]))
                        {
                            // verificar se eh a melhor posicao para inserir a tarefa nesta rota (maior tempo ocioso z)
                            if (z[j] > zStarRoute)
                            {
                                zStarRoute = z[j];                                                                                                           // tempo ocioso nao muda
                                tStarRoute = travelTime[j] + d[j][s.vec[i].sol] + c[s.vec[i].sol][route[j][k]] - d[j][route[j][k]];       // tempo de viagem atualizado
                                
                                existe = 1;
                                posRoute = k;
                                
                                inicioStarRoute = inicio;
                            }
                        }  
                    }
                    
                    // inserir entre duas tarefas (nao altera o tempo ocioso)
                    else
                    if (k<route[j].size())
                    {
                        // definir o inicio 
                        if (beginTime[route[j][k-1]]+task[route[j][k-1]].pt+c[route[j][k-1]][s.vec[i].sol] <= task[s.vec[i].sol].a)       // chegar antes da janela de inicio da tarefa (esperar)
                            inicio = task[s.vec[i].sol].a;
                        else                                                                                                            // chegar depois da janela de inicio da tarefa estar aberta
                            inicio = beginTime[route[j][k-1]]+task[route[j][k-1]].pt+c[route[j][k-1]][s.vec[i].sol]; 
                        
                        //verificar a janela de tempo
                        if ((inicio + task[s.vec[i].sol].pt <= task[s.vec[i].sol].b) && ((inicio + task[s.vec[i].sol].pt + c[s.vec[i].sol][route[j][k]]) <= beginTime[route[j][k]]))
                        {
                            // verificar se eh a melhor posicao para inserir a tarefa nesta rota (maior tempo ocioso z)
                            if (z[j] > zStarRoute)
                            {
                                zStarRoute = z[j];                                                                                                                                          // tempo ocioso nao muda
                                tStarRoute = travelTime[j] + c[route[j][k-1]][s.vec[i].sol] + c[s.vec[i].sol][route[j][k]] - c[route[j][k-1]][route[j][k]];    // tempo de viagem atualizado
                                
                                existe = 1;
                                posRoute = k;
                                
                                inicioStarRoute = inicio;
                            }
                        }  
                        
                    }
                    
                    // inserir apos a ultima tarefa
                    else
                    if (k == route[j].size())
                    {
                        // definir o inicio 
                        if (beginTime[route[j][k-1]]+task[route[j][k-1]].pt+c[route[j][k-1]][s.vec[i].sol] <= task[s.vec[i].sol].a)       // chegar antes da janela de inicio da tarefa (esperar)
                            inicio = task[s.vec[i].sol].a;
                        else                                                                                                            // chegar depois da janela de inicio da tarefa estar aberta
                            inicio = beginTime[route[j][k-1]]+task[route[j][k-1]].pt+c[route[j][k-1]][s.vec[i].sol]; 
                        
                        //verificar a janela de tempo
                        if ((inicio + task[s.vec[i].sol].pt <= task[s.vec[i].sol].b) && (inicio + task[s.vec[i].sol].pt + d[j][s.vec[i].sol] <= team[j].b))
                        {
                            // verificar se eh a melhor posicao para inserir a tarefa nesta rota (maior tempo ocioso z)
                            if (team[j].b - (inicio + task[s.vec[i].sol].pt + d[j][s.vec[i].sol]) > zStarRoute)
                            {
                                zStarRoute = team[j].b - (inicio + task[s.vec[i].sol].pt + d[j][s.vec[i].sol]);                                       // tempo ocioso 
                                tStarRoute = travelTime[j] + c[route[j][k-1]][s.vec[i].sol] + d[j][s.vec[i].sol] - d[j][route[j][k-1]];    // tempo de viagem atualizado
                                
                                existe = 1;
                                posRoute = k;
                                
                                inicioStarRoute = inicio;
                            }
                        }  
                    }
                    
                    // verificar se eh a melhor posicao para inserir a tarefa i na rota j
                    if (zStarRoute > zStarTeam && existe == 1)
                    {
                        zStarTeam = zStarRoute;
                        posStarTeam = posRoute;    
                        tStarTeam = tStarRoute;
                        inicioStarTeam = inicioStarRoute;
                    }
                        
                } // end route
                
                // ** ja sei em qual posicao inserir (bestPos), agora tenho que verificar se eh a melhor equipe para realizar
                
                // verificar se eh a melhor equipe para realizar a tarefa (eh viavel ?)
                if (tStarTeam < bestTime && existe == 1)
                {
                    bestTime = tStarTeam;
                    
                    bestTeam = j;                               // equipe que ira realizar a tarefa
                    bestPos = posStarTeam;                // posicao que a tarefa sera inserida
                    
                    bestZ = zStarTeam;
                    bestInicio = inicioStarTeam;
                    
                    achei = 1;
                }
                
            } // end if skill
        } // end team
        
        // insert task sol[i] in the best team and position
        if (achei == 1)
        {
            if (bestPos == route[bestTeam].size())
                route[bestTeam].push_back(s.vec[i].sol);
            else
                route[bestTeam].insert(route[bestTeam].begin()+bestPos, s.vec[i].sol);
            
            // update values of travelTime, z, beginTime
            travelTime[bestTeam] = bestTime;
            z[bestTeam] = bestZ;
            beginTime[s.vec[i].sol] = bestInicio;
        }
    
    } // end task
    
    
    // *** calculate fitness
    s.fo = 0; 
     
    for (int j=0; j<m; j++)
    {
        for (int i=0; i<route[j].size(); i++)
        {
            // priotity prizes
            s.fo += task[route[j][i]].w/MW;
            
            // idle time
            if (i == route[j].size()-1)
            {
                s.fo += (team[j].b - (beginTime[route[j][i]] + task[route[j][i]].pt + d[j][route[j][i]]))/MZ;
            }
        }
    }
    
    // transform in minimization problem
    s.fo = -s.fo;

    // save the best solution found in this run
    if (s.fo < bestSolution.fo)
    {
        bestSolution = s;
        CPUbest = clock();
    }

    // print
    if (print == 1)
    {   
        printf("\n\n");
        for (int j=0; j<m; j++)
        {
            printf("\nTeam %d: ", j);
            for(int i=0; i<route[j].size(); i++)
            {
                printf("%2d (%.2lf) - ", route[j][i], beginTime[route[j][i]]);
            }
            
            printf("\t [%.2lf] ", z[j]);
        }
        
        printf("\n\nFO: %.2lf \n\n", -s.fo);
    }

    // print in file
    if (print == 2)
    {   
        FILE *arquivo;
        arquivo = fopen("Solutions.txt","a");

        if (!arquivo)
        {
            printf("\n\nFile not found Solutions.txt!!!");
            getchar();
            exit(1);
        }
        
        for (int j=0; j<m; j++)
        {
            fprintf(arquivo,"\nTeam %d: ", j);
            for(int i=0; i<route[j].size(); i++)
            {
                fprintf(arquivo,"%2d (%.2lf) - ", route[j][i], beginTime[route[j][i]]);
            }
            
            fprintf(arquivo,"\t [%.2lf] ", z[j]);
        }
    }

    return s.fo;
}


TSol LocalSearch(TSol s)
{
	

	// save the best solution found in this run
    if (s.fo < bestSolution.fo)
    {
        bestSolution = s;
        CPUbest = clock();
    }

 	return s;
}

void ReadData(char nameTable[])
{
    char name[200] = "";
    strcat(name,nameTable);

    FILE *arq;
    arq = fopen(name,"r");

    if (arq == NULL)
    {
        printf("\nERROR: File (%s) not found!\n",name);
        getchar();
        exit(1);
    }

    // => name of instance
    strcpy(instance,nameTable);

    // => read data

    int temp;

	// number of tasks and teams
	fscanf(arq, "%d %d", &n, &m);

	//task and garage information
	task.clear();
	task.resize(n);

	team.clear();
	team.resize(m);

	// priority of tasks 
	fscanf(arq, "%d", &temp); // first value is not considered
	for (int i = 0; i < n; i++)
		fscanf(arq, "%d", &task[i].w);

	// skills between tasks and teams
	q.clear();
	q.resize(n, vector<int>(m));

	//fscanf(arq, "%d %d", &temp, &temp);
	for (int i = 0; i < m; i++) {
		fscanf(arq, "%d", &temp); // first line is not read
	}
	
	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++)
			fscanf(arq, "%d", &q[i][j]);

	// processing time of tasks 
	fscanf(arq, "%d", &temp);   // first value is not considered
	for (int i = 0; i < n; i++)
		fscanf(arq, "%d", &task[i].pt);

	// beginning time windows of tasks
	fscanf(arq, "%d", &temp);   // first value is not considered
	for (int i = 0; i < n; i++)
		fscanf(arq, "%d", &task[i].a);

	// end time windows of tasks
	fscanf(arq, "%d", &temp);   // first value is not considered
	for (int i = 0; i < n; i++)
		fscanf(arq, "%d", &task[i].b);

	// beginning time windows of teams
	for (int i = 0; i < m; i++)
		fscanf(arq, "%d", &team[i].a);

	// end time windows of teams
	for (int i = 0; i < m; i++)
		fscanf(arq, "%d", &team[i].b);

	fscanf(arq, "%d %d %d", &M, &MW, &MZ);


	// coordinates of teams (teams have the same location)
	double tempX, tempY;
	fscanf(arq, "%lf %lf ", &tempX, &tempY);
	for (int i = 0; i < m; i++)
	{
		team[i].x = tempX;
		team[i].y = tempY;
	}

	// coordinates of tasks
	for (int i = 0; i < n; i++)
		fscanf(arq, "%lf %lf ", &task[i].x, &task[i].y);

	fclose(arq);

	// calculate the Euclidean distances between tasks
	c.clear();
	c.resize(n, vector<double>(n));

	for (int i = 0; i < n; i++)
	{
		for (int j = i; j < n; j++)
		{
			c[i][j] = c[j][i] = sqrt((task[j].x - task[i].x) * (task[j].x - task[i].x) + (task[j].y - task[i].y) * (task[j].y - task[i].y));
		}
	}

	// calculate the Euclidean distances between task and teams garage
	d.clear();
	d.resize(m, vector<double>(n));

	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
		{
			d[i][j] = sqrt((task[j].x - team[i].x) * (task[j].x - team[i].x) + (task[j].y - team[i].y) * (task[j].y - team[i].y));
		}
	}
}

void FreeMemory()
{
    Pop.clear();
    PopInter.clear();
    c.clear();
    d.clear();
    q.clear();
    task.clear();
    team.clear();
}

/************************************************************************************
			                  GENERAL FUNCTIONS
*************************************************************************************/

void A_BRKGA()
{
    // max and min population size
    int pMax = 1000;
    int pMin = 100;

    // cooling rate [0.999, 0.998, 0.997]
    gama = 0.999;

    // define epoch interval
    teta = 0.01;

    // initial population size
    p = pMax;

    // initialize populations
    Pop.clear();
    PopInter.clear();

    Pop.resize(p);
    PopInter.resize(p);


    // Stop criterium
    maxGenerations = (pow(gama, (log(pMin)/log(gama) - pMax)));

    // top population size
    int Tpe;

    // top max population size
    int TpeMax;

    // number of chromossoms in top with no similarity
    int numTop;

    // mutation population size
    int Tpm;

    // number of local search applied in this generation
    int numLS = 0;

    // Create the initial chromossoms with random keys
    #pragma omp parallel for num_threads(MAX_THREADS)
    for (int i=0; i<p; i++)
    {
        TSol ind = CreateInitialSolutions();
        ind = Decoder(ind);
        Pop[p-i-1] = PopInter[p-i-1] = ind;
    }

    int numGenerations = 0;
    double mediaPop;
    float currentTime = 0;

    // run the evolutionary process until stop criterion
    while (numGenerations < maxGenerations)
    {
    	// number of generations
        numGenerations++;

        // sort population in increase order of fitness
        sort(Pop.begin(), Pop.end(), sortByFitness);

        // ** update parameters (p, pe and pm) **

        // population size: cooling annealing
        p = (int)(p * gama);
        Pop.resize(p);
        PopInter.resize(p);

        // increase max Elite
        pe = 0.10 + ((double)numGenerations/maxGenerations) * (0.25 - 0.10);
        TpeMax = (int)(p * pe);

        // candidate restrictive list of elite chromossoms
        alfa = 0.10 + (1 - (double)numGenerations/maxGenerations) * (0.30 - 0.10);
        double treashold = Pop[0].fo + alfa*(Pop[p-1].fo - Pop[0].fo);
        Tpe = 0;
        while (Pop[Tpe].fo <= treashold && Tpe < TpeMax)
            Tpe++;

        // decrease Mutations
        pm = 0.05 + (1-(double)numGenerations/maxGenerations) * (0.20 - 0.05);
        Tpm = (int)(p * pm);

        // ** end update **

        // We now will set every chromosome of 'current', iterating with 'i':
        int i = 0;	// Iterate chromosome by chromosome

        // number of chromossoms in Top with no similarity
        numTop = 0;

         // calculate media fitness population in the current generation
         mediaPop = 0;

        // The 'pe' best chromosomes (no similar) are maintained, so we just copy these into 'current':
        while(i < Tpe)
        {
            // initially, the chromossom i is not similar with the other chromossoms
            Pop[i].similar = 0;

            // copy the best chromossom for next generation
            if (i == 0)
            {
                PopInter[i] = Pop[i];
                numTop++;
                mediaPop += PopInter[i].fo;
            }

            // for ohter chromossom, calculate the similarity
            else
            {
                // calculate the similarity beetwen chromossoms i and k (chromossoms in top better than i)
                for (int k=0; k<i; k++)
                {
                    // chromossoms i and k are similar if they have the same fitness
                    if (Pop[i].fo == Pop[k].fo)
                    {
                        Pop[i].similar = 1;
                        break;
                    }
                }

                // copy chromossom i to the next generation if it is not simliar with other chromossoms
                if (Pop[i].similar == 0)
                {
                    PopInter[i] = Pop[i];
                    numTop++;
                    mediaPop += PopInter[i].fo;
                }

                // perturbation the top solution and copy to the next generation
                else
                {
                    PopInter[i] = Pop[i];

                    // perturbation intesity
                    beta = 0.10 + (Pop[i].vec[n+1].rk) * (0.20 - 0.10);

                    // apply perturbation method in a similar solution
                    PopInter[i] = Perturbation(PopInter[i], beta);

                    // calculate fitness
                    PopInter[i] = Decoder(PopInter[i]);
                    PopInter[i].flag = 0;  // set the flag of local search as zero
                    mediaPop += PopInter[i].fo;
                }
            }
        	++i;
        }

        // We'll mate 'p - pe - pm' pairs; initially, i = pe, so we need to iterate until i < p - pm:
        while(i < p - Tpm)
        {
        	// parametric uniform crossover
        	PopInter[i] = ParametricUniformCrossover(Tpe);

            ++i;
        }

        // We'll introduce 'pm' mutants:
        while(i < p)
        {
            PopInter[i] = CreateInitialSolutions();
            ++i;
        }

        // Calculate fitness
        #pragma omp parallel for num_threads(MAX_THREADS)
        for (i=Tpe; i<p; i++)
        {
            PopInter[i] = Decoder(PopInter[i]);
            mediaPop += PopInter[i].fo;
        }
        mediaPop = mediaPop/p;

        // Update the current population
        Pop = PopInter;


        // ***** Local search in communities ***
        if (ls)
        {
			numLS = 0;

        	// update epoch
        	int interval = (int)maxGenerations*0.10;
        	if ((numGenerations+1)%interval == 0)
		    	teta += 0.005;

	        int epoch = (int)maxGenerations*teta;

	        if (numGenerations%epoch == 0)
	        {
	            // Find commuties in Top chromossoms
	            sigma = 0.30 + ((double)numGenerations/maxGenerations) * (0.70 - 0.30);
	            IC(0, Tpe, sigma);

	            vector <int> promisingSol;
                promisingSol.clear();

	            for (i=0; i < Tpe; i++)
	            {
	                if (Pop[i].promising == 1)
	                {
	                	promisingSol.push_back(i);
	                    numLS++;
	                }
	            }

	            #pragma omp parallel for num_threads(MAX_THREADS)
                for (unsigned int i=0; i < promisingSol.size(); i++)
                {
                    // in this case, local search not influence the evolutionary process
                    LocalSearch(Pop[promisingSol[i]]);
                    Pop[promisingSol[i]].flag = 1;
                }

                promisingSol.clear();
	        }
	    }

        // ******** end local search ******

        if (debug)
            printf("\nGeneration: %3d [%3d - %3d(%.2lf) (%3d : %3d) - %3d(%.2lf)] \t %.2lf \t %.2lf \t %.2lf(%.0lf) ",
                        numGenerations, p, Tpe, pe, numTop, numLS, Tpm, pm, mediaPop, bestSolution.fo, teta, maxGenerations*teta);

		// restart population
        if (p < pMin)
        {
        	int currentP = p;

            // initial size population
            p = pMax;

            // create new chromossoms
            for (int k = 0; k < p-currentP; k++)
            {
                TSol ind = CreateInitialSolutions();
                Pop.push_back(Decoder(ind));
                PopInter.push_back(Decoder(ind));
            }
        }

        // terminate the evolutionary process in MAXTIME
        CPUend = clock();
        currentTime = (float)(CPUend - CPUbegin)/CLOCKS_PER_SEC;
        if (currentTime >= MAXTIME)
            break;
    }

    // free memory
    Pop.clear();
	PopInter.clear();
}

TSol CreateInitialSolutions()
{
	TSol s;
	TVecSol aux;

    s.vec.clear();

	// create a random-key for each allelo
	for (int j=0; j<n; j++)
	{
		aux.rk  = randomico(0,1);
		aux.sol = j;

        s.vec.push_back(aux);
	}

    // create an allelo for rhoe parameter
    aux.rk = randomico(0,1);
	aux.sol = -1;
	s.vec.push_back(aux);

    // create an allelo for beta parameter (perturbation)
    aux.rk = randomico(0,1);
	aux.sol = -1;
    s.vec.push_back(aux);

    // flag to control the local search memory
    s.flag = 0;

	return s;
}

TSol Perturbation(TSol s, double beta)
{
	for (int k=0; k<n; k++)
    {
        if (randomico(0,1) < beta)
        {
            // choose a random position
            int pos = irandomico(0, n-1);

            // swap genes k e pos
            double temp = s.vec[k].rk;
            s.vec[k].rk = s.vec[pos].rk;
            s.vec[pos].rk = temp;
        }
    }
    return s;
}

TSol ParametricUniformCrossover(int Tpe)
{
	TSol s;

	// create a new offspring
	s.vec.resize(n+2);

    // Select an elite parent:
    int eliteParent = irandomico(0,Tpe - 1);

    // Select a non-elite parent:
    int noneliteParent = Tpe + irandomico(0, p - Tpe - 1);

    // Define self-adaptative rhoe
    rhoe = 0.65 + (Pop[noneliteParent].vec[n].rk) * (0.80 - 0.65);

    // Mate:  // including rhoe and perturbation
    for(int j = 0; j < n+2; j++)
    {
        //copy alelos of top chromossom of the new generation
        if (randomico(0,1) < rhoe)
           s.vec[j].rk = Pop[eliteParent].vec[j].rk;
        else
           s.vec[j].rk = Pop[noneliteParent].vec[j].rk;

        if (j < n)
        	s.vec[j].sol = j;
        else
        	s.vec[j].sol = -1;
    }

    // set the flag of local search as zero
    s.flag = 0;

    return s;
}

double PearsonCorrelation(vector <TVecSol> X, vector<TVecSol> Y)
{
    double correlation = 0;
    double sumXY = 0;
    double sumX2 = 0;
    double sumY2 = 0;
    double sumX = 0;
    double sumY = 0;

    for(int j=0; j<n; j++)
    {
        sumX += X[j].rk;
        sumX2 += X[j].rk * X[j].rk;
        sumXY += X[j].rk * Y[j].rk;
        sumY += Y[j].rk;
        sumY2 += Y[j].rk * Y[j].rk;
    }

    //Pearson
    correlation= ((n*sumXY) - (sumX*sumY) ) / (sqrt( (n*sumX2 - sumX*sumX) * (n*sumY2 - sumY*sumY) ));
    return correlation;
}


void IC(int tInitial, int tEnd, double sigma)
{
    vector <vector <int> >  matArestas;
    int tam = tEnd - tInitial;

    // create similar matrix
    matArestas.clear();
    matArestas.resize(tam, vector<int>(tam));

    for (int i=tInitial; i<tEnd; i++)
    {
        for (int j=i; j<tEnd; j++)
        {
            if (i == j)
                matArestas[i][j] = 0;
            else
            {
                if (Pop[i].fo == Pop[j].fo)
                    matArestas[i][j] = matArestas[j][i] = 1;

                else
                {
                    if(PearsonCorrelation(Pop[i].vec, Pop[j].vec) > sigma)
                        matArestas[i][j] = matArestas[j][i] = 1;
                    else
                        matArestas[i][j] = matArestas[j][i] = 0;
                }
            }
        }
    }

    // apply clustering method
    LP(tInitial, tEnd, matArestas);
    PromisingLP(tInitial, tEnd);
}

void LP(int tInitial, int tEnd, vector< vector <int> > matArestas)
{
    // save the number of times a label appears in the neighbors of node i
    vector <int> v;
    v.resize(tEnd-tInitial);

    int escolhido = -1;
    vector <int> desempate;

    // initialize each node with its own label
    for(int i=tInitial; i<tEnd; i++)
        Pop[i].label = i;

    // search each graph node
    for(int i=tInitial; i<tEnd; i++)
    {
        // initialize v with 0
        for(unsigned int j=0; j<v.size(); j++)
            v[j]=0;

        // detects the most common id in the neighbors of node i
        for(int k=tInitial; k<tEnd; k++)
        {
            if(matArestas[i][k] == 1)
                v[Pop[k].label]++;
        }

        // number of times the most common label appears
        int maior=-1;
        for(int k=tInitial; k<tEnd; k++)
        {
            if(v[k] > maior)
                maior = v[k];
        }

        // random tiebreaker
        desempate.clear();
        for(unsigned int k=0; k<v.size(); k++)
        {
            if(v[k] == maior)
                desempate.push_back(k);
        }

        // define new label of node i
        escolhido = irandomico(0, desempate.size()-1);
        Pop[i].label = desempate[escolhido];
    }
}

void PromisingLP(int tInitial, int tEnd)
{
    vector <int> grupos;
    int tamanhoGrupos = 0;

    // initialize promisings solutions
    for(int i=tInitial; i<tEnd; i++)
        Pop[i].promising = 0;

    // save labels defined by LP in groups
    for(int i=tInitial; i<tEnd; i++)
    {
        int achei = 0;
        for(unsigned int j=0; j<grupos.size(); j++)
        {
            if(Pop[i].label == grupos[j])
                achei=1;
        }
        if(achei == 0)
        {
            tamanhoGrupos++;
            grupos.push_back(Pop[i].label);
        }
    }

    // find the best solution in the group (with flag = 0)
    for(unsigned int j=0; j<grupos.size(); j++)
    {
        double menorFO = INFINITY;
        int localMenor = -1;
        int local = -1;
        for(int i=tInitial; i<tEnd; i++)
        {
            if(Pop[i].label == grupos[j])
            {
                // find the best solution of the group
                if (local == -1)
                    local = i;

                if(Pop[i].fo < menorFO && Pop[i].flag == 0) // we not apply local search in this solution yet
                {
                    menorFO = Pop[i].fo;
                    localMenor = i;
                }
            }
        }

        if (localMenor == -1)
            localMenor = local;

        if(Pop[localMenor].label != -1)
            Pop[localMenor].promising = 1;
    }
}



/************************************************************************************
									IO Functions
*************************************************************************************/

void WriteSolutionScreen(TSol s)
{
	printf("\n\n\n Instance: %s \nsol: ", instance);
	for (int i=0; i<n; i++)
		printf("%d ", s.vec[i].sol);
	printf("\nfo: %.5lf",s.fo);
	printf("\nTotal time: %.3f",(float)(CPUend - CPUbegin)/CLOCKS_PER_SEC);
	printf("\nBest time: %.3f\n\n",(float)(CPUbest - CPUbegin)/CLOCKS_PER_SEC);

    // print problem solution
    print = 1;
    s.fo = CalculateFO(s);
}

void WriteSolution(TSol s)
{
	FILE *arquivo;
    arquivo = fopen("Solutions.txt","a");

	if (!arquivo)
	{
		printf("\n\nFile not found Solutions.txt!!!");
		getchar();
		exit(1);
	}

    fprintf(arquivo,"\n\nInstance: %s", instance);
	fprintf(arquivo,"\nSol: ");
	for (int i=0; i<n; i++)
		fprintf(arquivo,"%d ", s.vec[i].sol);
	fprintf(arquivo,"\nFO: %.5lf", s.fo);
  	fprintf(arquivo,"\nBest time: %.3f",(float)(CPUbest - CPUbegin)/CLOCKS_PER_SEC);
	fprintf(arquivo,"\nTotal time:%.3f \n",(float)(CPUend - CPUbegin)/CLOCKS_PER_SEC );

	fclose(arquivo);

    // print problem solution
    print = 2;
    s.fo = CalculateFO(s);
}

void WriteResults(double fo, double foAverage, float timeBest, float timeTotal)
{
	FILE *arquivo;
    arquivo = fopen("Results.xls","a");

	if (!arquivo)
	{
		printf("\n\nFile not found Results.xls!!!");
		getchar();
		exit(1);
	}

	fprintf(arquivo,"\n%s", instance);
	fprintf(arquivo,"\t%lf", fo);
	fprintf(arquivo,"\t%lf", foAverage);
	fprintf(arquivo,"\t%.3f", timeBest);
	fprintf(arquivo,"\t%.3f", timeTotal);

	fclose(arquivo);
}

double randomico(double min, double max)
{

	return ((double)(rand()%10000)/10000.0)*(max-min)+min;
}

int irandomico(int min, int max)
{

	return (int)randomico(0,max-min+1.0) + min;
}
